package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * JFree Common library for Icy
 * 
 * @author Stephane Dallongeville
 */
public class JFreeCommonPlugin extends Plugin implements PluginLibrary
{
    //
}
